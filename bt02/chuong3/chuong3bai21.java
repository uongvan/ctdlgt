/**
 * Created by Admin on 9/12/2016.
 */

import java.util.Scanner;

public class  chuong3bai21{
    public static void main(String[] args) {
        Scanner scanIn = new Scanner(System.in);
        int stake = scanIn.nextInt();// so tien co ban dau
        int goal = scanIn.nextInt();// so tien can dat duoc
        int T = scanIn.nextInt();// so lan choi
        int bets = 0;       //total number of bets made
        int wins = 0;       //total number of game won
        //repeat T time
        for (int t = 0; t < T; t++) {
            //do one gambler's ruin simulation
            int cash = stake;
           // while (cash > 0 && cash < goal)
            for(; cash > 0 && cash < goal; ){
                bets++;//so lượt choi trung binh
                if (Math.random() < 0.5) cash++;     //win 1$
                else cash--;     //lose $1
            }
            if (cash == goal) wins++;    //did gambler go achieve desired goal?
        }
        // print result
        System.out.println(wins + "wins of" + T);//so lan chien thang
        System.out.println("Percent of game won = " + 100.0 * wins / T);// xac suat thang
        System.out.println("Avg # bets          = " + 1.0 * bets / T);
    }
}
