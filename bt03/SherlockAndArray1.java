import java.util.Scanner;

public class SherlockAndArray1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int T = input.nextInt();

        for (int i = 0; i < T; i++) {
            int N = input.nextInt();
            int[] a = new int[N];
            int sum = 0;

                for (int j = 0; j < N; j++) {
                    a[j] = input.nextInt();
                    sum += a[j];
                }
            if(N==1) {
                System.out.println("YES");
            }
            else {
                int sum1 = a[0];
                int flag = 1;

                for (int j = 1; j < N; j++) {

                    if ((sum - a[j]) % 2 == 0 && (sum - a[j]) / 2 == sum1) {
                        System.out.println("YES");
                        flag = 0;
                        break;
                    }
                    sum1 += a[j];
                }

                if (flag == 1) {
                    System.out.println("NO");
                }
            }
        }
    }
}
