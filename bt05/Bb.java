import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Bb {
    public static void main(String[] args) {
        Stack st = new Stack();

        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for (int a0 = 0; a0 < t; a0++) {
            st = new Stack();
            String s = in.next();
            char a[] = s.toCharArray();
            int k=0;
            for (int i = 0; i < a.length; i++) {
                if (a[i] == '(' || a[i] == '{' || a[i] == '[') {
                    st.push(new Character(a[i]));
                } else {
                    if (st.empty()) {
                        System.out.println("NO");
                        k=1;
                        break;
                    } else {
                        Character item = (Character) st.pop();
                        if ((a[i] == ')' && item == '(') || (a[i] == ']' && item == '[') || (a[i] == '}' && item == '{')) {
                        } else {
                            st.push(new Character(item));

                            break;
                        }
                    }
                }
            }
            if(k == 0){
                if (st.empty()) {
                    System.out.println("YES");
                } else {
                    System.out.println("NO");
                }
            }
        }
    }
}
