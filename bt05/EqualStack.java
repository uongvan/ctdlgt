import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class EqualStack {

    public static int Max(int n1, int n2, int n3) {
        int n = Math.max(Math.max(n1, n2), n3);
        return n;
    }

    public static int Min(int n1, int n2, int n3) {
        int n = Math.min(Math.min(n1, n2), n3);
        return n;
    }

    public static int Equal(int s1, int s2, int s3) {
        if (s1 == s2 && s2 == s3) return 1;
        return -1;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n1 = in.nextInt();
        int n2 = in.nextInt();
        int n3 = in.nextInt();
        int h1[] = new int[n1];
        for (int h1_i = 0; h1_i < n1; h1_i++) {
            h1[h1_i] = in.nextInt();
        }
        int h2[] = new int[n2];
        for (int h2_i = 0; h2_i < n2; h2_i++) {
            h2[h2_i] = in.nextInt();
        }
        int h3[] = new int[n3];
        for (int h3_i = 0; h3_i < n3; h3_i++) {
            h3[h3_i] = in.nextInt();
        }
        int s1 = 0, s2 = 0, s3 = 0;
        for (int i = 0; i < n1; i++) {
            s1 += h1[i];
        }
        //System.out.println(s1);
        //System.out.println("chd" + h1[n1 - 1]);

        for (int i = 0; i < n2; i++) {
            s2 += h2[i];
        }
       // System.out.println(s2);

        for (int i = 0; i < n3; i++) {
            s3 += h3[i];
        }
//        System.out.println(s3);
        int i1=0, i2=0,i3=0;
        int n = Min(n1, n2, n3);
     //   System.out.println(n);
        int count = 0;
        while (Equal(s1, s2, s3) != 1 && s1 >=0 && s2 >=0 && s3 >=0) {
            if (s1 == Max(s1, s2, s3)) {
                s1 = s1 - h1[i1];
               // System.out.println("gfh");
                i1++;
            } else if (s2 == Max(s1, s2, s3)) {
                s2 = s2 - h2[i2];
                i2++;
            } else {
                s3 = s3 - h3[i3];
                i3++;
            }
            count++;
        }
        System.out.println(s3);

    }
}
