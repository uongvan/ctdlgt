import java.util.LinkedList;
import java.util.Scanner;
import java.util.Queue;


public class QueueUsingTwoStacks {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        Queue<Integer> queue = new LinkedList<Integer>();
        for (int i = 0; i < t; i++) {
            int a = sc.nextInt();
            if(a == 1){
                int s = sc.nextInt();
                queue.add(s);
            }
            else if(a == 2){
                queue.poll();
            } else if(a == 3){
                System.out.println(queue.peek());

            }
        }
    }
}

