import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class insertionsort1 {


    public static void insertIntoSorted(int[] ar) {
        // Fill up this function
        int n = ar.length - 1;
        //System.out.println(n);
        for (int j = n; j > 0; j--) {
            if (ar[j] <= ar[j - 1]) {
                int temp = ar[j];
                ar[j] = ar[j - 1];
                printArray(ar);
                ar[j - 1] = temp;
            } else {
                break;
            }
        }
    }



    /* Tail starts here */

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int s = in.nextInt();
        int[] ar = new int[s];
        for (int i = 0; i < s; i++) {
            ar[i] = in.nextInt();
        }
        insertIntoSorted(ar);
        printArray(ar);

    }


    private static void printArray(int[] ar) {
        for (int n : ar) {
            System.out.print(n + " ");
        }
        System.out.println("");
    }
}
 
